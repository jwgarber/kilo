#pragma once

// Headers should always compile on their own
// This means they always bring in the necessary types to use the header

#include <stdnoreturn.h>

noreturn void die(const char* const str);
