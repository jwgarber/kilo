#include <stdlib.h>
#include <string.h>

#include "die.h"
#include "abuf.h"

void abuf_init(struct abuf* const ab) {
    ab->buf = NULL;
    ab->len = 0;
}

void abuf_append(struct abuf* const ab, const char* const str) {

    const size_t len = strlen(str);

    // Get a new buffer
    char* const new_buf = realloc(ab->buf, ab->len + len);

    if (new_buf == NULL) {
        die("realloc");
    }

    memcpy(&new_buf[ab->len], str, len);
    ab->buf = new_buf;
    ab->len += len;
}

void abuf_free(struct abuf* const ab) {
    free(ab->buf);
}
