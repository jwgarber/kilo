TARGET = kilo

CC = musl-clang
STD = -std=c11 #-D_XOPEN_SOURCE=700
WARN = -Weverything -Wno-comma -Wno-disabled-macro-expansion -Wno-unused-command-line-argument
#WARN = -Wall -Wextra -Wpedantic
OPTS = -O3 -march=native -flto
CFLAGS = $(STD) $(WARN) $(OPTS) -g
#LDLIBS = -lpthread

SOURCES = $(wildcard *.c)
HEADERS = $(wildcard *.h)
OBJECTS = $(SOURCES:.c=.o)

# linking
$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDLIBS)

# compiling
%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean

clean:
	-rm -f $(OBJECTS) $(TARGET)
