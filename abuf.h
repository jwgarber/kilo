#pragma once

#include <stddef.h>     // size_t

// This string type is not zero-terminated

struct abuf {
    char* buf;
    size_t len;
};

void abuf_init(struct abuf* const ab);

void abuf_append(struct abuf* const ab, const char* const str);

void abuf_free(struct abuf* const ab);
