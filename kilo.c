#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <termios.h>
#include <unistd.h>

#include "editor.h"

// by default, terminal is in cooked mode, which allows the user to modify their input using enter and backspace.
// We want to turn this off and put the terminal in raw mode
// in this mode, keypresses are read instantly

// Ctrl-V sends next character literally
// Ctrl-C sends SIGINT
// Ctrl-D is EOF
// Ctrl-S stops output
// Ctrl-Q resumes output
// Ctrl-M sends a carriage return 13, which by default the terminal translates to a newline 10
// Ctrl-Z sends SIGTSTP

// An escape sequence starts with 27, and then a bunch of other bytes
// Page Up, Page Down, Home, End are escape sequences
// Backspace is 127, Delete is 4 byte escape sequence
// Escape sends a single 27 byte
// Enter is 10
// Ctrl-x sends 1-26, based on ord(x). But, some of these are special, like Ctrl-C.

// Static variables are zero-initialized for you
static struct termios orig_term;

// We register this function with atexit() to ensure that raw mode is disabled
// when the editor exits.
static void disable_raw_mode(void) {

    const int errval = tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_term);
    if (errval == -1) {
        perror("tcsetattr");
        // Don't call exit() here, since that is undefined behaviour
        // within an atexit() registered function and we are exiting
        // anyway.
    }
}

// see cfmakeraw also
static void enable_raw_mode(void) {

    // Save a copy of the original terminal state
    int errval = tcgetattr(STDIN_FILENO, &orig_term);
    if (errval == -1) {
        perror("tcgetattr");
        exit(EXIT_FAILURE);
    }

    // Register disable_raw_mode to ensure the original terminal settings are
    // restored on exit.
    errval = atexit(disable_raw_mode);
    if (errval != 0) {
        fputs("atexit registration failed\n", stderr);
        exit(EXIT_FAILURE);
    }

    struct termios raw_term = orig_term;

    // iflag - input flags
    // BRKINT does something with break conditions
    // ICRNL enables Ctrl-M
    // INPCK enables parity checking
    // ISTRIP sets 8th bit of each input byte to 0
    // IXON enables Ctrl-S and Ctrl-Q
    raw_term.c_iflag &= (unsigned int)~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    // oflag - output flags
    // OPOST enables output processing, like translating a '\n' to '\r\n'
    raw_term.c_oflag &= (unsigned int)~(OPOST);
    // c_cflag - control flags
    // CS8 sets character size to 8 bits per byte
    raw_term.c_cflag |= (CS8);
    // lflag - local flags
    // ECHO prints back the characters as you type them
    // ICANON is line-by-line mode
    // IEXTEN enables Ctrl-V
    // ISIG enables Ctrl-C and Ctrl-Z
    raw_term.c_lflag &= (unsigned int)~(ECHO | ICANON | IEXTEN | ISIG);

    // cc - control characters
    // VMIN - min number of bytes to read before read() returns. Setting to 0 means read() returns as soon as there is input
    raw_term.c_cc[VMIN] = 0;
    // VTIME - time before a read() times out. If read() times out, then returns 0 for no bytes read. Setting to 1 means 1/10 of second.
    // TODO do we need to have read() timeout?
    raw_term.c_cc[VTIME] = 1;

    // TCSAFLUSH waits for pending output to be written to the terminal and discards any input that hasn't been read before applying the changes
    errval = tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw_term);
    if (errval == -1) {
        perror("tcsetattr");
        exit(EXIT_FAILURE);
    }
}

int main(void) {

    enable_raw_mode();

    struct editor ed;
    editor_init(&ed);

    // We start to draw things on the screen once the main loop has begun,
    // and for that we want to clear the screen before exiting (hence call die)

    // To ensure we don't have undefined behaviour, only access stderr
    // through stdio, and stdin/stdout through read/write

    while (true) {
        editor_refresh_screen(&ed);
        editor_process_keypress(&ed);
    }
}

