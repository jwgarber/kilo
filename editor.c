#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stropts.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "abuf.h"
#include "die.h"
#include "editor.h"

#define KILO_VERSION "0.0.1"

// Bitwise ands a character with 00011111, which sets the upper three bits to 0 (which is what hitting Ctrl does in the terminal)
// This has to be a macro, because we use it in a switch
#define CTRL_KEY(k) ((k) & 0x1F)

enum escape_sequence {
    arrow_left = 1024,
    arrow_right,
    arrow_up,
    arrow_down,
    page_up,
    page_down,
    home,
    end,
    delete,
};

static int read_key(void) {

    char c;
    ssize_t nread;

    // Block, waiting until a character is entered
    while (nread = read(STDIN_FILENO, &c, 1), nread != 1) {
        if (nread == -1) {
            die("read");
        }
    }

    if (c != '\x1B') {
        // Just an ordinary character, so return it
        return c;
    }

    // If we read an escape character, attempt to read the next few characters to see
    // if it is an escape sequence. If it is an escape sequence but isn't recognized,
    // then just return escape.
    char seq[3];

    nread = read(STDIN_FILENO, &seq[0], 1);
    if (nread == -1) {
        die("read");
    }

    // Nothing more read, so it was just an escape character
    if (nread != 1) {
        return c;
    }

    if (seq[0] == '[') {
        // Then this is an control sequence, so continue reading

        nread = read(STDIN_FILENO, &seq[1], 1);
        if (nread == -1) {
            die("read");
        }

        // Nothing more read, so just return escape
        if (nread != 1) {
            return c;
        }

        switch (seq[1]) {
            case 'A': return arrow_up;
            case 'B': return arrow_down;
            case 'C': return arrow_right;
            case 'D': return arrow_left;
            case 'H': return home;
            case 'F': return end;
        }

        // It wasn't an arrow key, so try other sequences
        if ('0' <= seq[1] && seq[1] <= '9') {

            // Read next character
            nread = read(STDIN_FILENO, &seq[2], 1);
            if (nread == -1) {
                die("read");
            }

            // Nothing more read, so just return escape
            if (nread != 1) {
                return c;
            }

            if (seq[2] == '~') {
                switch (seq[1]) {
                    case '1': return home;
                    case '3': return delete;
                    case '4': return end;
                    case '5': return page_up;
                    case '6': return page_down;
                    case '7': return home;
                    case '8': return end;
                }
            }
        }
    }

    // Else just fall-through to escape
    return c;
}

// This function is called before the main loop, so we don't use die() here
static void get_window_size(size_t* const rows, size_t* const cols) {

    // Returning structs is inefficient in C, so store the return values in pointers
    struct winsize ws;

    const int errval = ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);

    if (errval == -1) {
        perror("ioctl");
        exit(EXIT_FAILURE);
    }

    // Double check that the window size isn't zero.
    if (ws.ws_col == 0 || ws.ws_row == 0) {
        fputs("winsize rows or cols are zero\n", stderr);
        exit(EXIT_FAILURE);
    }

    *cols = ws.ws_col;
    *rows = ws.ws_row;
}

// When subtracting unsigned integers, always always make sure it does not underflow

static void draw_rows(const struct editor* const ed, struct abuf* const ab) {

    for (size_t y = 0; y < ed->screen_rows - 1; ++y) {
        if (y == ed->screen_rows / 2) {
            const char* const welcome = "Kilo editor -- version " KILO_VERSION;
            const size_t welcome_len = strlen(welcome);

            // If the welcome length is less than the number of screen columns, we add a ~ and then pad the welcome message
            if (welcome_len < ed->screen_cols) {
                abuf_append(ab, "~");

                // Pad the rest
                // Subtract 1 for the ~
                size_t padding = (ed->screen_cols - welcome_len - 1) / 2;

                while (padding != 0) {
                    abuf_append(ab, " ");
                    padding--;
                }
            }

            abuf_append(ab, welcome);

        } else {
            abuf_append(ab, "~");
        }

        // K clears the line to the right of the cursor
        abuf_append(ab, "\x1b[K\r\n");
    }

    abuf_append(ab, "~\x1b[K");
}

void editor_init(struct editor* const ed) {
    ed->cursor_x = 0;
    ed->cursor_y = 0;
    get_window_size(&ed->screen_rows, &ed->screen_cols);
}

void editor_refresh_screen(const struct editor* const ed) {

    struct abuf ab;
    abuf_init(&ab);

    // Hide the cursor
    abuf_append(&ab, "\x1b[?25l");

    // \x1b is the escape character (27)
    // Put cursor in top left corner
    abuf_append(&ab, "\x1b[H");

    draw_rows(ed, &ab);

    // Position to move the cursor to
    // Terminal is 1-indexed
    // TODO double check this buffer length
    char buf[32];
    const int rval = snprintf(buf, sizeof(buf), "\x1b[%zu;%zuH", ed->cursor_y + 1, ed->cursor_x + 1);

    if (rval < 0) {
        die("snprintf");
    }

    abuf_append(&ab, buf);

    // Show the cursor
    abuf_append(&ab, "\x1b[?25h");

    write(STDOUT_FILENO, ab.buf, ab.len);

    abuf_free(&ab);
}

static void move_cursor(struct editor* const ed, const int key) {
    switch (key) {
        case arrow_left:
            if (ed->cursor_x != 0) {
                ed->cursor_x--;
            }
            break;
        case arrow_right:
            if (ed->cursor_x != ed->screen_cols - 1) {
                ed->cursor_x++;
            }
            break;
        case arrow_up:
            if (ed->cursor_y != 0) {
                ed->cursor_y--;
            }
            break;
        case arrow_down:
            if (ed->cursor_y != ed->screen_rows - 1) {
                ed->cursor_y++;
            }
            break;
    }
}

// Move cursor to the top or bottom of the page for now
static void move_page(struct editor* const ed, const int key) {

    const int arrow = key == page_up ? arrow_up : arrow_down;

    for (size_t times = ed->screen_rows; times != 0; --times) {
        move_cursor(ed, arrow);
    }
}

void editor_process_keypress(struct editor* const ed) {

    const int c = read_key();

    switch (c) {
        case CTRL_KEY('q'):
            // Clear the screen
            write(STDOUT_FILENO, "\x1b[2J", 4);
            write(STDOUT_FILENO, "\x1b[H", 3);
            exit(EXIT_SUCCESS);

        case arrow_up:
        case arrow_down:
        case arrow_left:
        case arrow_right:
            move_cursor(ed, c);
            break;

        case page_up:
        case page_down:
            move_page(ed, c);
            break;

        case home:
            ed->cursor_x = 0;
            break;
        case end:
            ed->cursor_x = ed->screen_cols - 1;
            break;
    }
}


