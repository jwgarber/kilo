#pragma once

#include <stddef.h>

struct editor {
    // 0-based cursor position
    // Always 0 <= cursor < screen_cols
    size_t cursor_x;
    size_t cursor_y;
    // An invariant is that these two values must be >= 1
    size_t screen_cols; // number of columns
    size_t screen_rows; // number of rows
};

void editor_init(struct editor* const ed);

void editor_refresh_screen(const struct editor* const ed);

void editor_process_keypress(struct editor* const ed);
