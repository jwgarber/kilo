#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "die.h"

// If the editor encounters an error and needs to quit, we want to clear
// the screen and reposition the cursor before printing the error message.
noreturn void die(const char* const str) {

    // LOLOLOLOL, these write calls can change errno even if they succeed
    const int err = errno;

    write(STDOUT_FILENO, "\x1b[2J", 4);     // Clear the screen
    write(STDOUT_FILENO, "\x1b[H", 3);      // Move the cursor to the origin

    errno = err;
    perror(str);
    exit(EXIT_FAILURE);
}
